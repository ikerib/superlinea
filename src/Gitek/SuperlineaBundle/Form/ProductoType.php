<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ProductoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            // ->add('lineas', 'collection', array(
            //     'type' => new LineaType(),
            //     'allow_add' => true,
            //     'allow_delete' => true,
            //     'by_reference' => false,
            // ))
            // ->add('lineas', 'entity', array(
            //     'class'         => 'SuperlineaBundle:Linea',
            //     'property'      => 'nombre',
            //     'expanded'      => true,
            //     'multiple'      => true,
            //     'query_builder' => function(EntityRepository $er)
            //     {
            //         return $er->createQueryBuilder('l')
            //         ->where ('l.mostrar=1')
            //         ->orderBy('l.nombre','ASC');
            //     },
            // ))

            // ->add('materiales', 'entity', array(
            //     'class'         => 'SuperlineaBundle:Material',
            //     'property'      => 'nombre',
            //     'expanded'      => true,
            //     'multiple'      => true,
            //     'query_builder' => function(EntityRepository $er)
            //     {
            //         return $er->createQueryBuilder('l')
            //         ->orderBy('l.nombre','ASC');
            //     },
            // ))

            // ->add('instrucciones', 'entity', array(
            //     'class'         => 'SuperlineaBundle:Instruccion',
            //     'property'      => 'nombre',
            //     'expanded'      => true,
            //     'multiple'      => true,
            //     'query_builder' => function(EntityRepository $er)
            //     {
            //         return $er->createQueryBuilder('i')
            //         ->orderBy('i.nombre','ASC');
            //     },
            // ))

            // ->add('materiales', 'collection', array(
            //     'type' => new MaterialselectType(),
            //     'allow_add' => true,
            //     'allow_delete' => true,
            //     'by_reference' => false,
            // ));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Producto'
        ));
    }

    public function getName()
    {
        return 'producto';
    }
}
