<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MaterialselectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',
                'entity',
                array(
                    'class' => 'SuperlineaBundle:Material',
                    // 'class' => 'Gitek\\SuperlineaBundle\\Entity\\Linea',
                    // 'empty_value' => 'Selecciona una linea',

                )
            )
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Material'
        ));
    }

    public function getName()
    {
        return 'gitek_superlineabundle_materialtype';
    }
}
