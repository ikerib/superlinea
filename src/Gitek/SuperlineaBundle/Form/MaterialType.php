<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class MaterialType extends AbstractType
{
    protected $miref;
    public function __construct ($miref)
    {
        $this->miref = $miref;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ref = $this->miref;

        $builder
            ->add('nombre')
            ->add('referencia')

            ->add('image', 'file', array(
                                    'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                                    'required' => false,
            ))
//        ->add('image')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
//        $resolver->setDefaults(array(
//            'data_class' => 'Gitek\SuperlineaBundle\Entity\Material',
//            'ref' => ''
//        ));
//        $resolver->setOptional(array('ref'));
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Material',
            'ref' => ''
        ));
    }


    public function getName()
    {
        return 'gitek_superlineabundle_materialtype';
    }
}
