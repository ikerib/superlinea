<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PuestoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('linea', 'entity', array(
                'class'         => 'SuperlineaBundle:Linea',
                'property'      => 'nombre',
                'expanded'      => false,
                'multiple'      => false,
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('l')
                    ->where ('l.mostrar=1')
                    ->orderBy('l.nombre','ASC');
                },
            ))

            // ->add('linea')
            // ->add('instruccion')
            ->add('producto')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Puesto'
        ));
    }

    public function getName()
    {
        return 'puesto';
    }
}
