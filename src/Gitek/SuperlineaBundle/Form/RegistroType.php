<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class RegistroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('producto')
            // Solo los productos que tengan configuración
            ->add('producto', 'entity', array(
                'class' => 'SuperlineaBundle:Producto',
                'property' => 'nombre',
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                            ->innerJoin('p.puestos', 'pu');
                    },
            ))
            ->add('lineas', 'entity', array(
                'class' => 'SuperlineaBundle:Linea',
                'property' => 'nombre',
                'expanded' => false,
                'multiple' => true,
                'required' => true,
                'empty_data'  => null,
                'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('l')
                            ->where('l.mostrar=1')
                            ->orderBy('l.nombre', 'ASC');
                    },
            ))
            ->add('fecha', 'date',
                array(
                    'attr' => array('class' => 'somedatefield'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                )
            )
            ->add('fechafin', 'date',
                array(
                    'attr' => array('class' => 'somedatefield'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false
                )
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Registro'
        ));
    }

    public function getName()
    {
        return 'gitek_superlineabundle_registrotype';
    }
}
