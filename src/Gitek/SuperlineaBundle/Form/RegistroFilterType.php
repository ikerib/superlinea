<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class RegistroFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('producto', 'entity', array(
                'required' => false,
                'class' => 'SuperlineaBundle:Producto',
                'property' => 'nombre',
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                            ->innerJoin('p.puestos', 'pu');
                    },
            ))
            ->add('lineas', 'entity', array(
                'required' => false,
                'class' => 'SuperlineaBundle:Linea',
                'property' => 'nombre',
                'expanded' => false,
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('l')
                            ->where('l.mostrar=1')
                            ->orderBy('l.nombre', 'ASC');
                    },
            ))
            ->add('fecha', 'date',
                array(
                    'empty_value' => '',
                    'required' => false,
                    'attr' => array('class' => 'somedatefield'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                )
            )
            ->add('fechafin', 'date',
                array(
                    'empty_value' => '',
                    'required' => false,
                    'attr' => array('class' => 'somedatefield'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                )
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Registro'
        ));
    }

    public function getName()
    {
        return 'registrofiltertype';
    }
}
