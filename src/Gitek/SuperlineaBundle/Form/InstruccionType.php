<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class InstruccionType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ref = $options['ref'];

        $builder
            ->add('id','hidden')
            ->add('nombre')
            ->add('proceso')

            ->add('referencia')


            ->add('image', 'file', array(
                'label' => 'Imagen:',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required' => false,
            ))
            ->add('videofile', 'file', array(
                'label' => 'Video:',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required' => false,

            ))

            ->add('youtube')
            ->add('materiales', 'entity', array(
                    'class' => 'SuperlineaBundle:Material',
                    'attr' => array('class' => 'checkbox'),
                    'expanded' => true,
                    'multiple' => true,
                    'required' => false,
                    'query_builder' => function (EntityRepository $er) use ($ref) {
                            if ( $ref !== "") {
                                $q = $er->createQueryBuilder('m')
                                    ->where('m.referencia=:ref')
                                    ->setParameter('ref', $ref);
                                return $q;
                            } else {
                                $q = $er->createQueryBuilder('m');
                                return $q;
                            }
                    })
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Instruccion',
            'ref' => ''
        ));
    }

    public function getName()
    {
        return 'gitek_superlineabundle_instrucciontype';
    }
}
