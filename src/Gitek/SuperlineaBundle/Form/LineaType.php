<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class LineaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('mostrar', 'choice', array(
                'choices' => array('1' => 'Si', '0' => 'No')
            ))

            // Deskomentatu hau select bezela azaltzeko
            // ->add('nombre',
            //     'entity',
            //     array(
            //         'class' => 'SuperlineaBundle:Linea',
            //         // 'class' => 'Gitek\\SuperlineaBundle\\Entity\\Linea',
            //         // 'empty_value' => 'Selecciona una linea',
            //         'query_builder' => function(EntityRepository $repositorio)
            //         {
            //             return $repositorio->createQueryBuilder('l')
            //                     ->where("l.mostrar = '1' ")
            //                     ->orderBy('l.nombre', 'ASC');
            //         },
            //     )
            // )
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Linea'
        ));
    }

    public function getName()
    {
        return 'linea';
    }
}
