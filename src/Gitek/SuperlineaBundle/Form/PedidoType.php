<?php

namespace Gitek\SuperlineaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PedidoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('fecha')
            ->add('fecha','date',
                array(
                'attr' => array('class' => 'somedatefield'),
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                    )
                )
            ->add('entregado', 'choice', array(
                'choices' => array('1' => 'Si', '0' => 'No')
            ))
            // ->add('material')
            // ->add('puesto')
            // ->add('producto')
            // ->add('linea')
            // ->add('usuario')
            // ->add('instrucciones')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\SuperlineaBundle\Entity\Pedido'
        ));
    }

    public function getName()
    {
        return 'gitek_superlineabundle_pedidotype';
    }
}
