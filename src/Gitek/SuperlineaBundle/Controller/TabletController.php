<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\SuperlineaBundle\Entity\Tablet;
use Gitek\SuperlineaBundle\Form\TabletType;

/**
 * Tablet controller.
 *
 */
class TabletController extends Controller
{
    /**
     * Lists all Tablet entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SuperlineaBundle:Tablet')->findAll();

        return $this->render('SuperlineaBundle:Tablet:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Tablet entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Tablet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tablet entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Tablet:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Tablet entity.
     *
     */
    public function newAction()
    {
        $entity = new Tablet();
        $form   = $this->createForm(new TabletType(), $entity);

        return $this->render('SuperlineaBundle:Tablet:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Tablet entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Tablet();
        $form = $this->createForm(new TabletType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // return $this->redirect($this->generateUrl('tablet_show', array('id' => $entity->getId())));
            return $this->redirect($this->generateUrl('tablet'));
        }

        return $this->render('SuperlineaBundle:Tablet:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Tablet entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Tablet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tablet entity.');
        }

        $editForm = $this->createForm(new TabletType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Tablet:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Tablet entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Tablet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tablet entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TabletType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tablet'));
            // return $this->redirect($this->generateUrl('tablet_edit', array('id' => $id)));
        }

        return $this->render('SuperlineaBundle:Tablet:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Tablet entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        // if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SuperlineaBundle:Tablet')->find($id);
            // $this->get('session')->setFlash(
            //     'notice',
            //     'Ha sido eliminado!'
            // );
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tablet entity.');
            }

            $em->remove($entity);
            $em->flush();
        // }

        return $this->redirect($this->generateUrl('tablet'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
