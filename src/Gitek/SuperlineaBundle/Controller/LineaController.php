<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\SuperlineaBundle\Entity\Linea;
use Gitek\SuperlineaBundle\Form\LineaType;

/**
 * Linea controller.
 *
 */
class LineaController extends Controller
{
    /**
     * Lists all Linea entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SuperlineaBundle:Linea')->findAll();
        return $this->render('SuperlineaBundle:Linea:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Linea entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Linea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Linea entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Linea:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Linea entity.
     *
     */
    public function newAction()
    {
        $entity = new Linea();
        $form   = $this->createForm(new LineaType(), $entity);

        return $this->render('SuperlineaBundle:Linea:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Linea entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Linea();
        $form = $this->createForm(new LineaType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('linea'));
            // return $this->redirect($this->generateUrl('linea_show', array('id' => $entity->getId())));
        }

        return $this->render('SuperlineaBundle:Linea:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Linea entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Linea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Linea entity.');
        }

        $editForm = $this->createForm(new LineaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Linea:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Linea entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Linea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Linea entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new LineaType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('linea_edit', array('id' => $id)));
        }

        return $this->render('SuperlineaBundle:Linea:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Linea entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        // if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SuperlineaBundle:Linea')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Linea entity.');
            }

            $em->remove($entity);
            $em->flush();
        // }

        return $this->redirect($this->generateUrl('linea'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
