<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\SuperlineaBundle\Entity\Registro;
use Gitek\SuperlineaBundle\Form\RegistroType;
use Gitek\SuperlineaBundle\Form\RegistroFilterType;

/**
 * Registro controller.
 *
 */
class RegistroController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Registro();
        $form = $this->createForm(new RegistroFilterType(), $entity);

        if ($request->getMethod() === 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $searchCriteria = $form->getData();
                $entities = $em->getRepository('SuperlineaBundle:Registro')->getListBy($searchCriteria);
            }
        } else {
            $entities = $em->getRepository('SuperlineaBundle:Registro')->findAll();
        }
        return $this->render('SuperlineaBundle:Registro:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
        ));
    }

    public function createAction(Request $request)
    {
        // Comprobamos que no exista un lanzamiento previo programado
        // para el producto-linea-fecha seleccionado
        $em = $this->getDoctrine()->getManager();
        $entity = new Registro();
        $form = $this->createForm(new RegistroType(), $entity);
        $form->bind($request);
        if ($form->isValid()) {
            $entity2 = new Registro();
            $form2 = $this->createForm(new RegistroFilterType(), $entity2);
            $searchCriteria = $form->getData();
            $aurkituak = $em->getRepository('SuperlineaBundle:Registro')->getListBy($searchCriteria);
            if (count($aurkituak) > 0) {
                return $this->redirect($this->generateUrl('lanzador', array('existe' => '1')));
            }

            if ( $searchCriteria->getFecha() == null ) {
                $entity->setFecha(new \DateTime());
            }

            if ( $searchCriteria->getFechafin() == null ) {
                $entity->setFechafin(new \DateTime());
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lanzador'));
        }

        return $this->render('SuperlineaBundle:Registro:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Registro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Registro:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }

    /**
     * Displays a form to create a new Registro entity.
     *
     */
    public function newAction()
    {
        $entity = new Registro();
        $form = $this->createForm(new RegistroType(), $entity);

        return $this->render('SuperlineaBundle:Registro:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Registro entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Registro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registro entity.');
        }

        $editForm = $this->createForm(new RegistroType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Registro:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Registro entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Registro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new RegistroType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            // return $this->redirect($this->generateUrl('registro_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('registro'));
        }

        return $this->render('SuperlineaBundle:Registro:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction($id, $token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SuperlineaBundle:Registro')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Centro entity.');
        }
        $csrf = $this->container->get('form.csrf_provider');

        if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token)) {
            $em->remove($entity);
            $em->flush();
        } else {
            throw $this->createNotFoundException('Token es incorrecto.');
        }

        return $this->redirect($this->generateUrl('lanzador'));

    }
}
