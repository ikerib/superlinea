<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;

use Gitek\SuperlineaBundle\Entity\Material;
use Gitek\SuperlineaBundle\Form\MaterialType;

/**
 * Material controller.
 *
 */
class MaterialController extends Controller
{
    /**
     * Lists all Material entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SuperlineaBundle:Material')->findAll();

        return $this->render('SuperlineaBundle:Material:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Material entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Material')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Material entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Material:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Material entity.
     *
     */
    public function newAction()
    {
        $entity = new Material();
        $form   = $this->createForm(new MaterialType(), $entity);

        return $this->render('SuperlineaBundle:Material:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Material entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Material();
        $form = $this->createForm(new MaterialType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            if ( $this->existeReferencia($entity->getReferencia()) == true ) {
                $form->get('referencia')->addError(new FormError('La referencia ya existe.'));
                return $this->render('SuperlineaBundle:Material:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));
            }

            if ( $entity->getReferencia() == null ) {
                $form->get('referencia')->addError(new FormError('Tienes que especificar una referencia.'));
                return $this->render('SuperlineaBundle:Material:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('material'));
            // return $this->redirect($this->generateUrl('material_show', array('id' => $entity->getId())));
        }

        return $this->render('SuperlineaBundle:Material:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Material')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Material entity.');
        }

        $editForm = $this->createForm(new MaterialType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Material:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Material')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Material entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new MaterialType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            if ( $this->existeReferencia($entity->getReferencia()) == true ) {
                $editForm->get('referencia')->addError(new FormError('La referencia ya existe.'));
                return $this->render('SuperlineaBundle:Material:edit.html.twig', array(
                    'entity'      => $entity,
                    'edit_form'   => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));
            }

            if ( $entity->getReferencia() == null ) {
                $editForm->get('referencia')->addError(new FormError('Tienes que especificar una referencia.'));
                return $this->render('SuperlineaBundle:Material:edit.html.twig', array(
                    'entity'      => $entity,
                    'edit_form'   => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));
            }

            $entity->setUpdatedAt(new \DateTime());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('material_edit', array('id' => $id)));
        }

        return $this->render('SuperlineaBundle:Material:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        // if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SuperlineaBundle:Material')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Material entity.');
            }

            $em->remove($entity);
            $em->flush();
        // }

        return $this->redirect($this->generateUrl('material'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    private function existeReferencia($ref) {
        $em = $this->getDoctrine()->getManager();

        $bilaketa = $em->getRepository('SuperlineaBundle:Material')->findReferencia($ref);

        if ( !$bilaketa ) {
            return false;
        }

        return true;
    }

    private function existeMaterialParaProceso($codbar, $proceso) {
        $em = $this->getDoctrine()->getManager();

        $bilaketa = $em->getRepository('SuperlineaBundle:Material')->findmaterialporcodbaryproceso($codbar, $proceso);

        if ( !$bilaketa ) {
            return false;
        }

        return true;
    }

}
