<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SuperlineaController extends Controller
{

    public function dashboardAction()
    {
        return $this->render('SuperlineaBundle:Superlinea:dashboard.html.twig', array(

        ));
    }

}
