<?php

namespace Gitek\SuperlineaBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Routing\ClassResourceInterface;

use Gitek\SuperlineaBundle\Entity\Puesto;
use Gitek\SuperlineaBundle\Entity\Pedido;
use Gitek\SuperlineaBundle\Form\PuestoType;

ini_set('max_execution_time', 300);

/**
 * API del Puesto controller.
 *
 */
class PuestoapiController extends Controller
{

    public function getMaterialfilterAction($ref)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $materiales = $em->getRepository('SuperlineaBundle:Material')->findReferencia($ref);

        $serializador = $this->container->get('serializer');
        return new Response($serializador->serialize($materiales, 'json'));
    } // "get_materialfilter"   [GET] /materialfilter/{ref}

    public function deletePuestoAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $puesto = $em->getRepository('SuperlineaBundle:Puesto')->find($id);
        $productoid = $puesto->getProducto()->getId();


        $em->remove($puesto);
        $em->flush();

        $serializador = $this->container->get('serializer');
        return new Response($serializador->serialize($productoid, 'json'));
    } // "delete_puesto"   [DELETE] /puestos/{id}


    public function putPuestoAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $puesto = $em->getRepository('SuperlineaBundle:Puesto')->find($id);

        if (!$puesto) {
            throw $this->createNotFoundException('Unable to find Puesto puesto.');
        }

        $put_str = $this->getRequest()->getContent();

        parse_str($put_str, $_PUT);


        $mix = $_PUT['x'];

        $miy = $_PUT['y'];

        $miinstruccion = $_PUT['instruccionid'];

        if (isset($miinstruccion) && ($miinstruccion != "")) {

            $instruccion = $em->getRepository('SuperlineaBundle:Instruccion')->find($miinstruccion);
            if ($instruccion) {
                $puesto->setInstruccion($instruccion);

            }
        }
        $tabletid = $_PUT['tabletid'];

        if (isset($tabletid) && ($tabletid != "")) {
            $tablet = $em->getRepository('SuperlineaBundle:Tablet')->find($tabletid);
            $puesto->setTablet($tablet);
        }

        $usuarioid = $_PUT['usuarioid'];

        if (isset($usuarioid) && ($usuarioid != "")) {

            $usuario = $em->getRepository('SuperlineaBundle:Usuario')->find($usuarioid);
            $puesto->setUsuario($usuario);
        }

        $puesto->setX($mix);
        $puesto->setY($miy);
        $em->persist($puesto);
        $em->flush();

        $response = new Response();

        $response->setStatusCode(200);

        return $response;
    } // "put_puesto"      [PUT] /puestos/{id}

    public function postPuestosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $put_str = $this->getRequest()->getContent();
        parse_str($put_str, $_PUT);

        $lineaid = $_PUT['lineaid'];
        $productoid = $_PUT['productoid'];

        $linea = $em->getRepository("SuperlineaBundle:Linea")->findOneBy(array('nombre' => $lineaid, 'mostrar' => '1'));

        $producto = $em->getRepository("SuperlineaBundle:Producto")->find($productoid);

        $puesto = New Puesto();
        $puesto->setLinea($linea);
        $puesto->setProducto($producto);

        $em->persist($puesto);
        $em->flush();

        $serializador = $this->container->get('serializer');


        return new Response($serializador->serialize($puesto, 'json'));
    } // "post_puestos"      [POST] /puestos

    public function postPedidosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $put_str = $this->getRequest()->getContent();
        parse_str($put_str, $_PUT);

        $materialid = $request->request->get('materialid');

        $puestoid = $request->request->get('puestoid');

        $productoid = $request->request->get('productoid');

        $lineaid = $request->request->get('lineaid');

        $usuarioid = $request->request->get('usuarioid');


        $material = $em->getRepository("SuperlineaBundle:Material")->find($materialid);
        $puesto = $em->getRepository("SuperlineaBundle:Puesto")->find($puestoid);
        $producto = $em->getRepository("SuperlineaBundle:Producto")->find($productoid);
        $linea = $em->getRepository("SuperlineaBundle:Linea")->find($lineaid);
        $usuario = $em->getRepository("SuperlineaBundle:Usuario")->find($usuarioid);

        $pedido = New Pedido();
        $pedido->setMaterial($material);
        $pedido->setPuesto($puesto);
        $pedido->setProducto($producto);
        $pedido->setLinea($linea);
        $pedido->setUsuario($usuario);
        $kolorea = "";
        $fec = date('y-m-d');
        $pedidos = $em->getRepository("SuperlineaBundle:Pedido")->cantidadpedidos($fec);

        $zenbat = count($pedidos);

        switch ($zenbat) {
            case 0:
                $kolorea = "kuadro-gorria";
                break;
            case 1:
                $kolorea = "kuadro-berdea";
                break;
            case 2:
                $kolorea = "kuadro-urdin";
                break;
            default:
                $kolorea = "kuadro-beltza";
                break;
        }

        $pedido->setKolorea($kolorea);

        $em->persist($pedido);
        $em->flush();

        $erantzuna = array(
            "kolorea" => $kolorea,
            "pedidoid" => $pedido->getId(),
            "materialfoto" => $material->getFoto()
        );
        $serializador = $this->container->get('serializer');

        return new Response($serializador->serialize($erantzuna, 'json'));
    } // "post_pedidos"      [POST] /pedidos

    public function putPedidoAction($id)
    {


        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $pedido = $em->getRepository('SuperlineaBundle:Pedido')->find($id);

        if (!$pedido) {
            throw $this->createNotFoundException('Unable to find pedido .');
        }
        if ($pedido->getLeido() == 0) {
            return new Response ("{ respuesta:Noleido }", 204);
        }
        $pedido->setEntregado(1);
        $em->persist($pedido);
        $em->flush();

        return new Response("{ respuesta:OK }");
    } // "put_pedido"      [PUT] /pedidos/{id}

    public function getPedidosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $fecha = date('Y-m-d');

        $pedidos = $em->getRepository('SuperlineaBundle:Pedido')->cantidadpedidos($fecha);

        if (!$pedidos) {
            return new Response("NO");
        } else {
            $pedido = $pedidos[0];
        }

        $zenbat = count($pedidos);
        $kolorea = "";
        switch ($zenbat) {
            case 0:
                $kolorea = "kuadro-gorria";
                break;
            case 1:
                $kolorea = "kuadro-berdea";
                break;
            case 2:
                $kolorea = "kuadro-urdin";
                break;
            default:
                $kolorea = "kuadro-beltza";
                break;
        }

        // $pedido->setKolorea($kolorea);

        $erantzuna = array("kolorea" => $kolorea, "pedido" => $pedido);

        $serializador = $this->container->get('serializer');

        return new Response($serializador->serialize($erantzuna, 'json'));

    } // "get_pedidos"     [GET] /pedidos

    public function getPedidoAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('SuperlineaBundle:Pedido')->pedidopendiente($id);

        if (!$pedido) {
            $pedido = null;
        }

        $serializador = $this->container->get('serializer');

        $respuesta = new Response($serializador->serialize($pedido, 'json'));
        $respuesta->headers->set('Content-Type', 'application/json');
        return $respuesta;

    } // "get_pedido"      [GET] /pedidos/{id}

    public function deletePedidoAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $puesto = $em->getRepository('SuperlineaBundle:Pedido')->find($id);
        $em->remove($puesto);
        $em->flush();

        $response = new Response();

        $response->setStatusCode(200);

        return $response;
    } // "delete_pedido"   [DELETE] /pedidos/{id}

    public function getMaterialAction($codbar)
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $pedidoid = $request->query->get('pedidoid');
        $material = $em->getRepository('SuperlineaBundle:Material')->findmaterialporcodbar($codbar, $pedidoid);

        $puestos = null;

        if (!$material) {
            $material = null;
        } else {
            $pedido = $em->getRepository('SuperlineaBundle:Pedido')->find($pedidoid);
            $pedido->setLeido(1);
            $em->persist($pedido);
            $em->flush();
            $lineaid = $pedido->getLinea()->getId();
            $productoid = $pedido->getProducto()->getId();
            $puestos = $em->getRepository('SuperlineaBundle:Material')->findpuestos($pedidoid, $lineaid, $productoid);
        }

        $serializador = $this->container->get('serializer');

        $erantzuna = array();
        $erantzuna['Material'] = $material;
        $erantzuna['Puestos'] = $puestos;

        return new Response($serializador->serialize($erantzuna, 'json'));

    } // "get_material"      [GET] /material/{codbar}

    public function getInstruccionesAction($nombre)
    {
        $em = $this->getDoctrine()->getManager();

        $fecha =  new \Datetime();
        $instrucciones = $em->getRepository('SuperlineaBundle:Instruccion')->instruccionespatatablet($nombre, $fecha);


        if (!$instrucciones) {
            $instrucciones = null;
        }

        $serializador = $this->container->get('serializer');

        $respuesta = new Response($serializador->serialize($instrucciones, 'json'));
        $respuesta->headers->set('Content-Type', 'application/json');
        return $respuesta;
    }
    // "get_instrucciones"      [GET] /instruccion/{nombre}
}
