<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Gitek\SuperlineaBundle\Entity\Pedido;
use Gitek\SuperlineaBundle\Form\PedidoType;

/**
 * Pedido controller.
 *
 */
class PedidoController extends Controller
{

  public function pedidopendienteAction($puestoid) {
    $em = $this->getDoctrine()->getManager();
    // ladybug_dump( $puestoid );
    $pedido = $em->getRepository('SuperlineaBundle:Pedido')->pedidopendienteentrega($puestoid);

    // ladybug_dump( $pedido );
    if (!$pedido) {
      $pedido=null;
    }

    $serializador = $this->container->get('serializer');

    $respuesta = new Response($serializador->serialize($pedido, 'json'));
    $respuesta->headers->set('Content-Type', 'application/json');
    return $respuesta;
  }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        // $fecha = date('Y-m-d');
        // $pedidos = $em->getRepository('SuperlineaBundle:Pedido')->cantidadpedidos($fecha);
        $pedidos = $em->getRepository('SuperlineaBundle:Pedido')->findAll();
        // $pedidos = $em->getRepository('SuperlineaBundle:Pedido')->cantidadpedidos($fecha);
        return $this->render('SuperlineaBundle:Pedido:index.html.twig', array(
            'entities' => $pedidos,
        ));
    }

    public function islaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fecha = date('y-m-d');
        $entities = $em->getRepository('SuperlineaBundle:Pedido')->findAll(array('fecha' => $fecha));

        return $this->render('SuperlineaBundle:Pedido:isla.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Pedido entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Pedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pedido entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Pedido:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Pedido entity.
     *
     */
    public function newAction()
    {
        $entity = new Pedido();
        $form   = $this->createForm(new PedidoType(), $entity);

        return $this->render('SuperlineaBundle:Pedido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Pedido entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Pedido();
        $form = $this->createForm(new PedidoType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pedido_show', array('id' => $entity->getId())));
        }

        return $this->render('SuperlineaBundle:Pedido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pedido entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Pedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pedido entity.');
        }

        $editForm = $this->createForm(new PedidoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Pedido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Pedido entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Pedido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pedido entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new PedidoType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pedido'));
        }

        return $this->render('SuperlineaBundle:Pedido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Pedido entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SuperlineaBundle:Pedido')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pedido entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pedido'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
