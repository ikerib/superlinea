<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;

use Gitek\SuperlineaBundle\Entity\Puesto;
use Gitek\SuperlineaBundle\Form\PuestoType;

class PuestoController extends Controller
{

   public function configAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $mytab = $request->get('mytab');

        $lineas = $em->getRepository('SuperlineaBundle:Linea')->findAll();
        if (!isset($mytab)) {
            $count = count($lineas);
            for ($i=0; $i < $count; $i++) {
                if ($lineas[$i]->getMostrar()==1) {
                    $mytab = $lineas[$i]->getNombre();
                    break;
                }
            }
        }

        $tablets       = $em->getRepository('SuperlineaBundle:Tablet')->tabletsinasignar($id);
        $usuarios      = $em->getRepository('SuperlineaBundle:Usuario')->usuariossinasignar($id,$mytab);
        $instrucciones = $em->getRepository('SuperlineaBundle:Instruccion')->findAll();
        $referencias   = $em->getRepository('SuperlineaBundle:Instruccion')->findReferencias();
        $producto      = $em->getRepository('SuperlineaBundle:Producto')->find($id);

        if (!$producto) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        return $this->render('SuperlineaBundle:Puesto:config.html.twig', array(
            'producto'      => $producto,
            'lineas'        => $lineas,
            'tablets'       => $tablets,
            'usuarios'      => $usuarios,
            'instrucciones' => $instrucciones,
            'mytab'         => $mytab,
            'referencias'   => $referencias
        ));
    }

    public function createAction(Request $request)
    {
        $entity  = new Puesto();
        $form = $this->createForm(new PuestoType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // return $this->redirect($this->generateUrl('Puesto_show', array('id' => $entity->getId())));
            return $this->redirect($this->generateUrl('puesto_config', array('id' => $entity->getProducto()->getId())));
        }

        return $this->render('SuperlineaBundle:Puesto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

}
