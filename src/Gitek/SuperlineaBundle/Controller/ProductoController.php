<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

use Gitek\SuperlineaBundle\Entity\Producto;
use Gitek\SuperlineaBundle\Form\ProductoType;
use Gitek\SuperlineaBundle\Entity\Puesto;
use Gitek\SuperlineaBundle\Form\PuestoType;
use Gitek\SuperlineaBundle\Entity\Registro;
use Gitek\SuperlineaBundle\Form\RegistroType;

/**
 * Producto controller.
 *
 */
class ProductoController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SuperlineaBundle:Producto')->findAll();

        return $this->render('SuperlineaBundle:Producto:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function lanzadorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $existe = $request->query->get('existe');

        $productos = $em->getRepository('SuperlineaBundle:Producto')->findAll();
        $lineas = $em->getRepository('SuperlineaBundle:Linea')->findAll();
        $actuales = $em->getRepository('SuperlineaBundle:Registro')->getregistroactual();

        $registro = new Registro();
        $form = $this->createForm(new RegistroType(), $registro);

        return $this->render('SuperlineaBundle:Producto:lanzador.html.twig', array(
            'productos' => $productos,
            'lineas' => $lineas,
            'actuales' => $actuales,
            'existe' => $existe,
            'form' => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Producto:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }

    public function newAction()
    {
        $entity = new Producto();
        $form = $this->createForm(new ProductoType(), $entity);

        return $this->render('SuperlineaBundle:Producto:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function createAction(Request $request)
    {
        $entity = new Producto();
        $form = $this->createForm(new ProductoType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // return $this->redirect($this->generateUrl('producto_show', array('id' => $entity->getId())));
            $this->get('session')->getFlashBag()->add('success', 'Producto creado con exito.');
            return $this->redirect($this->generateUrl('producto'));
        }

        return $this->render('SuperlineaBundle:Producto:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Producto')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('danger', 'Producto no encontrado.');
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $editForm = $this->createForm(new ProductoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Producto:edit.html.twig', array(
            'entity' => $entity,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function configAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $mytab = $request->get('mytab');

        $lineas = $em->getRepository('SuperlineaBundle:Linea')->findAll();

        if (!isset($mytab)) {
            $count = count($lineas);
            for ($i = 0; $i < $count; $i++) {
                if ($lineas[$i]->getMostrar() == 1) {
                    $mytab = $lineas[$i]->getNombre();
                }
            }
        }

        $producto = $em->getRepository('SuperlineaBundle:Producto')->find($id);

        $puesto = new Puesto();
        $formPuesto = $this->createForm(new PuestoType(), $puesto);

        if (!$producto) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $editForm = $this->createForm(new ProductoType(), $producto);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Producto:config.html.twig', array(
            'producto' => $producto,
            'lineas' => $lineas,
            'formPuesto' => $formPuesto->createView(),
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'mytab' => $mytab,
        ));
    }

    public function configupdateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ProductoType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('producto_config', array('id' => $id)));
        }

        return $this->render('SuperlineaBundle:Producto:config.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Producto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ProductoType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Datos del producto actualizados con éxito.');
            return $this->redirect($this->generateUrl('producto_index'));
            // return $this->redirect($this->generateUrl('producto_edit', array('id' => $id)));
        }

        return $this->render('SuperlineaBundle:Producto:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        // if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SuperlineaBundle:Producto')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('danger', 'Producto no encontrado.');
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }

        $em->remove($entity);
        $em->flush();
        // }

        $this->get('session')->getFlashBag()->add('success', 'Producto eliminado.');
        return $this->redirect($this->generateUrl('producto'));
    }
}
