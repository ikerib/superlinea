<?php

namespace Gitek\SuperlineaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\SuperlineaBundle\Entity\Instruccion;
use Gitek\SuperlineaBundle\Form\InstruccionType;

class InstruccionController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SuperlineaBundle:Instruccion')->findAll();

        return $this->render('SuperlineaBundle:Instruccion:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function soytabletAction($nombre)
    {
        $em = $this->getDoctrine()->getManager();

        $fecha = date("Y-m-d");
        $instrucciones = $em->getRepository('SuperlineaBundle:Instruccion')->instruccionespatatablet($nombre, $fecha);
        $puestos = $em->getRepository('SuperlineaBundle:Instruccion')->puesto($nombre, $fecha);


        if (!$puestos) {
            $puesto = null;
        } else {
            $puesto = $puestos[0];
        }

        if (!$instrucciones) {
            $instrucciones = null;
        }

        return $this->render('SuperlineaBundle:Instruccion:instruccion.html.twig', array(
            'instrucciones' => $instrucciones,
            'puesto' => $puesto,
        ));
    }

    public function getinstruccionesAction($ref)
    {
        $em = $this->getDoctrine()->getManager();

        if ($ref == "null") {
            $referencias = $em->getRepository('SuperlineaBundle:Instruccion')->findAll();
        } else {
            $referencias = $em->getRepository('SuperlineaBundle:Instruccion')->filtraReferencias($ref);
        }

        if (!$referencias) {
            throw $this->createNotFoundException('Unable to find Instruccion referencias.');
        }

        $serializador = $this->container->get('serializer');
        $response = new Response($serializador->serialize($referencias, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Instruccion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instruccion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Instruccion:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),));
    }

    public function newAction()
    {
        $entity = new Instruccion();
        $form = $this->createForm(new InstruccionType(), $entity);

        return $this->render('SuperlineaBundle:Instruccion:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function createAction(Request $request)
    {
        $entity = new Instruccion();
        $form = $this->createForm(new InstruccionType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('instruccion'));
            // return $this->redirect($this->generateUrl('instruccion_show', array('id' => $entity->getId())));
        }

        return $this->render('SuperlineaBundle:Instruccion:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, $ref="")
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Instruccion')->find($id);
        $referencias = $em->getRepository('SuperlineaBundle:Material')->findReferencias();



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instruccion entity.');
        }

        $editForm = $this->createForm(new InstruccionType(), $entity, array('ref'=>$ref));
//        $editForm = $this->createForm(new InstruccionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SuperlineaBundle:Instruccion:edit.html.twig', array(
            'entity' => $entity,
            'referencias' => $referencias,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

     public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuperlineaBundle:Instruccion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instruccion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new InstruccionType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $entity->setUpdatedAt(new \DateTime());
            $em->persist($entity);
            $em->flush();
            // ladybug_dump($entity);
            return $this->redirect($this->generateUrl('instruccion_edit', array('id' => $id)));
        }

        return $this->render('SuperlineaBundle:Instruccion:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        // if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SuperlineaBundle:Instruccion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instruccion entity.');
        }

        $em->remove($entity);
        $em->flush();
        // }

        return $this->redirect($this->generateUrl('instruccion'));
    }

    public function deleteuploadAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $instruccion = $em->getRepository('SuperlineaBundle:Instruccion')->find($id);

        if ($instruccion) {
            $instruccion->setVideo(null);
            $instruccion->setUpdatedAt(new \DateTime());
            $em->persist($instruccion);
            $em->flush();
            return new Response('OK', 200);
        } else {
            return new Response('KO', 404);
        }
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }
}
