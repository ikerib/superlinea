<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Gitek\SuperlineaBundle\Entity\Pedido
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\SuperlineaBundle\Entity\PedidoRepository")
 * @ExclusionPolicy("all")
 */
class Pedido
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var \DateTime $fecha
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var smallint $leido
     *
     * @ORM\Column(name="leido", type="smallint", nullable=true)
     * @Expose
     */
    private $leido;

    /**
     * @var string $kolorea
     *
     * @ORM\Column(name="kolorea", type="string", length=255)
     * @Expose
     */
    private $kolorea;


    /**
     * @var smallint $entregado
     *
     * @ORM\Column(name="entregado", type="smallint", nullable=true)
     */
    private $entregado;

    /**
     * @ORM\ManyToOne(targetEntity="Material", inversedBy="pedidos")
     * @ORM\JoinColumn(name="material_id", referencedColumnName="id")
     * @Expose
     */
    protected $material;

    /**
     * @ORM\ManyToOne(targetEntity="Puesto", inversedBy="pedidos")
     * @ORM\JoinColumn(name="puesto_id", referencedColumnName="id")
     */
    protected $puesto;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="pedidos")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Linea", inversedBy="pedidos")
     * @ORM\JoinColumn(name="linea_id", referencedColumnName="id")
     */
    protected $linea;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="pedidos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;

   /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToMany(targetEntity="Instruccion", mappedBy="materiales")
     */
    private $instrucciones;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->fecha = new \DateTime();
        $this->entregado = 0;
        $this->leido = 0;
    }

    public function __toString()
    {
        return $this->getMaterial()->getNombre();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pedido
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Pedido
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Pedido
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set material
     *
     * @param Gitek\SuperlineaBundle\Entity\Material $material
     * @return Pedido
     */
    public function setMaterial(\Gitek\SuperlineaBundle\Entity\Material $material = null)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return Gitek\SuperlineaBundle\Entity\Material
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set puesto
     *
     * @param Gitek\SuperlineaBundle\Entity\Puesto $puesto
     * @return Pedido
     */
    public function setPuesto(\Gitek\SuperlineaBundle\Entity\Puesto $puesto = null)
    {
        $this->puesto = $puesto;

        return $this;
    }

    /**
     * Get puesto
     *
     * @return Gitek\SuperlineaBundle\Entity\Puesto
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * Set producto
     *
     * @param Gitek\SuperlineaBundle\Entity\Producto $producto
     * @return Pedido
     */
    public function setProducto(\Gitek\SuperlineaBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return Gitek\SuperlineaBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set linea
     *
     * @param Gitek\SuperlineaBundle\Entity\Linea $linea
     * @return Pedido
     */
    public function setLinea(\Gitek\SuperlineaBundle\Entity\Linea $linea = null)
    {
        $this->linea = $linea;

        return $this;
    }

    /**
     * Get linea
     *
     * @return Gitek\SuperlineaBundle\Entity\Linea
     */
    public function getLinea()
    {
        return $this->linea;
    }

    /**
     * Set usuario
     *
     * @param Gitek\SuperlineaBundle\Entity\Usuario $usuario
     * @return Pedido
     */
    public function setUsuario(\Gitek\SuperlineaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return Gitek\SuperlineaBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Add instrucciones
     *
     * @param Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones
     * @return Pedido
     */
    public function addInstruccione(\Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones)
    {
        $this->instrucciones[] = $instrucciones;

        return $this;
    }

    /**
     * Remove instrucciones
     *
     * @param Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones
     */
    public function removeInstruccione(\Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones)
    {
        $this->instrucciones->removeElement($instrucciones);
    }

    /**
     * Get instrucciones
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getInstrucciones()
    {
        return $this->instrucciones;
    }

    /**
     * Set entregado
     *
     * @param integer $entregado
     * @return Pedido
     */
    public function setEntregado($entregado)
    {
        $this->entregado = $entregado;

        return $this;
    }

    /**
     * Get entregado
     *
     * @return integer
     */
    public function getEntregado()
    {
        return $this->entregado;
    }

    /**
     * Set leido
     *
     * @param integer $leido
     * @return Pedido
     */
    public function setLeido($leido)
    {
        $this->leido = $leido;

        return $this;
    }

    /**
     * Get leido
     *
     * @return integer
     */
    public function getLeido()
    {
        return $this->leido;
    }

    /**
     * Set kolorea
     *
     * @param string $kolorea
     * @return Pedido
     */
    public function setKolorea($kolorea)
    {
        $this->kolorea = $kolorea;
    
        return $this;
    }

    /**
     * Get kolorea
     *
     * @return string 
     */
    public function getKolorea()
    {
        return $this->kolorea;
    }
}
