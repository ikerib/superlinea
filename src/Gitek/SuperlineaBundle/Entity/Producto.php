<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Gitek\SuperlineaBundle\Entity\Producto
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gitek\SuperlineaBundle\Entity\ProductoRepository")
 * @ExclusionPolicy("all")
 */
class Producto
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Expose
     */
    private $nombre;


    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="Puesto", mappedBy="producto", cascade={"remove"})
     */
    private $puestos;

   /**
     * @ORM\OneToMany(targetEntity="Registro", mappedBy="producto", cascade={"remove"})
     */
    private $registros;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="producto", cascade={"remove"})
     */
    private $pedidos;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->puestos = new ArrayCollection();
        $this->registros = new ArrayCollection();
        $this->pedidos = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNombre();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Producto
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Producto
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add puestos
     *
     * @param Gitek\SuperlineaBundle\Entity\Puesto $puestos
     * @return Producto
     */
    public function addPuesto(\Gitek\SuperlineaBundle\Entity\Puesto $puestos)
    {
        $this->puestos[] = $puestos;

        return $this;
    }

    /**
     * Remove puestos
     *
     * @param Gitek\SuperlineaBundle\Entity\Puesto $puestos
     */
    public function removePuesto(\Gitek\SuperlineaBundle\Entity\Puesto $puestos)
    {
        $this->puestos->removeElement($puestos);
    }

    /**
     * Get puestos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPuestos()
    {
        return $this->puestos;
    }

    /**
     * Add registros
     *
     * @param Gitek\SuperlineaBundle\Entity\Registro $registros
     * @return Producto
     */
    public function addRegistro(\Gitek\SuperlineaBundle\Entity\Registro $registros)
    {
        $this->registros[] = $registros;

        return $this;
    }

    /**
     * Remove registros
     *
     * @param Gitek\SuperlineaBundle\Entity\Registro $registros
     */
    public function removeRegistro(\Gitek\SuperlineaBundle\Entity\Registro $registros)
    {
        $this->registros->removeElement($registros);
    }

    /**
     * Get registros
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getRegistros()
    {
        return $this->registros;
    }

    /**
     * Add pedidos
     *
     * @param Gitek\SuperlineaBundle\Entity\Pedido $pedidos
     * @return Producto
     */
    public function addPedido(\Gitek\SuperlineaBundle\Entity\Pedido $pedidos)
    {
        $this->pedidos[] = $pedidos;

        return $this;
    }

    /**
     * Remove pedidos
     *
     * @param Gitek\SuperlineaBundle\Entity\Pedido $pedidos
     */
    public function removePedido(\Gitek\SuperlineaBundle\Entity\Pedido $pedidos)
    {
        $this->pedidos->removeElement($pedidos);
    }

    /**
     * Get pedidos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPedidos()
    {
        return $this->pedidos;
    }
}
