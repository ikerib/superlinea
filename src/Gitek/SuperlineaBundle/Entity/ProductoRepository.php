<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\EntityRepository;


class ProductoRepository extends EntityRepository
{
  public function puestoporlinea($productoid, $mytab) {

    $em = $this->getEntityManager();

    $consulta = $em->createQuery('SELECT p FROM SuperlineaBundle:Producto p
                                JOIN p.puestos pu
                                JOIN pu.lineas l
                                WHERE p.id = :productoid
                                AND l.nombre = :mytab
                                ');

    $consulta->setParameter('productoid', $productoid);
    $consulta->setParameter('mytab', $mytab);

    $resul = $consulta->getOneOrNullResult();

    return $resul;
  }

  public function productosconfigurados() {

    $em = $this->getEntityManager();

    $consulta = $em->createQuery('SELECT p FROM SuperlineaBundle:Producto p
                                inner join p.puestos pu
                                ');

    return $consulta->getResult();
  }


}
