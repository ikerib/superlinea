<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Doctrine\Common\Collections\ArrayCollection;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Gitek\SuperlineaBundle\Entity\Material
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\SuperlineaBundle\Entity\MaterialRepository")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 * @ExclusionPolicy("all")
 */
class Material
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Expose
     */
    private $nombre;

    /**
     * @var string $referencia
     *
     * @ORM\Column(name="referencia", type="string", length=255, nullable=true)
     * @Expose
     */
    private $referencia;

    /**
     * @var string $codbar
     *
     * @ORM\Column(name="codbar", type="string", length=255, nullable=true)
     */
    private $codbar;

    /**
     * @Assert\File(
     *     maxSize="1M",
     *     mimeTypes={"image/png"}
     * )
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="foto", nullable=true)
     *
     * @var File $image
     */
    private $image;

    /**
     * @var string $foto
     *
     * @ORM\Column(name="foto", type="string", length=255, nullable=true)
     * @Expose
     */
    private $foto;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToMany(targetEntity="Instruccion", mappedBy="materiales")
     */
    private $instrucciones;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="material", cascade={"remove"})
     */
    private $pedidos;

    public function setImage($image) {
        $this -> image = $image;
        if ($image instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getImage() {
        return $this->image;
    }

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->pedidos = new ArrayCollection();

    }

    public function __toString()
    {
        return $this->getNombre();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Material
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Material
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Material
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Material
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add instrucciones
     *
     * @param Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones
     * @return Material
     */
    public function addInstruccione(\Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones)
    {
        $this->instrucciones[] = $instrucciones;

        return $this;
    }

    /**
     * Remove instrucciones
     *
     * @param Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones
     */
    public function removeInstruccione(\Gitek\SuperlineaBundle\Entity\Instruccion $instrucciones)
    {
        $this->instrucciones->removeElement($instrucciones);
    }

    /**
     * Get instrucciones
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getInstrucciones()
    {
        return $this->instrucciones;
    }

    /**
     * Add pedidos
     *
     * @param Gitek\SuperlineaBundle\Entity\Pedido $pedidos
     * @return Material
     */
    public function addPedido(\Gitek\SuperlineaBundle\Entity\Pedido $pedidos)
    {
        $this->pedidos[] = $pedidos;

        return $this;
    }

    /**
     * Remove pedidos
     *
     * @param Gitek\SuperlineaBundle\Entity\Pedido $pedidos
     */
    public function removePedido(\Gitek\SuperlineaBundle\Entity\Pedido $pedidos)
    {
        $this->pedidos->removeElement($pedidos);
    }

    /**
     * Get pedidos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPedidos()
    {
        return $this->pedidos;
    }

    /**
     * Set codbar
     *
     * @param string $codbar
     * @return Material
     */
    public function setCodbar($codbar)
    {
        $this->codbar = $codbar;

        return $this;
    }

    /**
     * Get codbar
     *
     * @return string
     */
    public function getCodbar()
    {
        return $this->codbar;
    }


    /**
     * Set referencia
     *
     * @param string $referencia
     * @return Material
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }
}
