<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
/**
 * Gitek\SuperlineaBundle\Entity\Puesto
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gitek\SuperlineaBundle\Entity\PuestoRepository")
 * @ExclusionPolicy("all")
 */
class Puesto
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255,  nullable=true)
     * @Expose
     */
    private $nombre;

    /**
     * @var decimal $x
     *
     * @ORM\Column(name="x", type="decimal",  nullable=true)
     */
    protected $x;

    /**
     * @var decimal $y
     *
     * @ORM\Column(name="y", type="decimal", nullable=true)
     */
    protected $y;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Linea", inversedBy="puestos")
     * @ORM\JoinColumn(name="linea_id", referencedColumnName="id")
     * @Expose
     */
    protected $linea;

    /**
     * @ORM\ManyToOne(targetEntity="Tablet", inversedBy="puestos")
     * @ORM\JoinColumn(name="tablet_id", referencedColumnName="id")
     */
    protected $tablet;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="puestos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * @Expose
     */
    protected $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Instruccion", inversedBy="puestos")
     * @ORM\JoinColumn(name="instruccion_id", referencedColumnName="id")
     */
    protected $instruccion;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="puestos")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     * @Expose
     */
    protected $producto;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="puesto", cascade={"remove"})
     */
    private $pedidos;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->pedidos = new ArrayCollection();

        $this->x=0;
        $this->y=0;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set x
     *
     * @param float $x
     * @return Puesto
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return float
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param float $y
     * @return Puesto
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return float
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Puesto
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Puesto
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set linea
     *
     * @param Gitek\SuperlineaBundle\Entity\Linea $linea
     * @return Puesto
     */
    public function setLinea(\Gitek\SuperlineaBundle\Entity\Linea $linea = null)
    {
        $this->linea = $linea;

        return $this;
    }

    /**
     * Get linea
     *
     * @return Gitek\SuperlineaBundle\Entity\Linea
     */
    public function getLinea()
    {
        return $this->linea;
    }

    /**
     * Set tablet
     *
     * @param Gitek\SuperlineaBundle\Entity\Tablet $tablet
     * @return Puesto
     */
    public function setTablet(\Gitek\SuperlineaBundle\Entity\Tablet $tablet = null)
    {
        $this->tablet = $tablet;

        return $this;
    }

    /**
     * Get tablet
     *
     * @return Gitek\SuperlineaBundle\Entity\Tablet
     */
    public function getTablet()
    {
        return $this->tablet;
    }

    /**
     * Set instruccion
     *
     * @param Gitek\SuperlineaBundle\Entity\Instruccion $instruccion
     * @return Puesto
     */
    public function setInstruccion(\Gitek\SuperlineaBundle\Entity\Instruccion $instruccion = null)
    {
        $this->instruccion = $instruccion;

        return $this;
    }

    /**
     * Get instruccion
     *
     * @return Gitek\SuperlineaBundle\Entity\Instruccion
     */
    public function getInstruccion()
    {
        return $this->instruccion;
    }

    /**
     * Set producto
     *
     * @param Gitek\SuperlineaBundle\Entity\Producto $producto
     * @return Puesto
     */
    public function setProducto(\Gitek\SuperlineaBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return Gitek\SuperlineaBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Puesto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set usuario
     *
     * @param Gitek\SuperlineaBundle\Entity\Usuario $usuario
     * @return Puesto
     */
    public function setUsuario(\Gitek\SuperlineaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return Gitek\SuperlineaBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Add pedidos
     *
     * @param Gitek\SuperlineaBundle\Entity\Pedido $pedidos
     * @return Puesto
     */
    public function addPedido(\Gitek\SuperlineaBundle\Entity\Pedido $pedidos)
    {
        $this->pedidos[] = $pedidos;

        return $this;
    }

    /**
     * Remove pedidos
     *
     * @param Gitek\SuperlineaBundle\Entity\Pedido $pedidos
     */
    public function removePedido(\Gitek\SuperlineaBundle\Entity\Pedido $pedidos)
    {
        $this->pedidos->removeElement($pedidos);
    }

    /**
     * Get pedidos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPedidos()
    {
        return $this->pedidos;
    }
}
