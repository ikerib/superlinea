<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Gitek\SuperlineaBundle\Entity\Instruccion
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gitek\SuperlineaBundle\Entity\InstruccionRepository")
 * @Vich\Uploadable
 * @ExclusionPolicy("all")
 */
class Instruccion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Expose
     */
    private $nombre;

    /**
     * @var string $proceso
     *
     * @ORM\Column(name="proceso", type="string", length=255, nullable=true)
     * @Expose
     */
    private $proceso;

    /**
     * @var string $referencia
     *
     * @ORM\Column(name="referencia", type="string", length=255, nullable=true)
     * @Expose
     */
    private $referencia;

    /**
     * @Assert\File(
     *     maxSize="1M",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="foto", nullable=true)
     *
     * @var File $image
     */
    protected $image;

    /**
     * @var string $foto
     *
     * @ORM\Column(name="foto", type="string", length=255, nullable=true)
     * @Expose
     */
    protected $foto;

    /**
     * @Assert\File(
     *     maxSize="10M",
     *     mimeTypes={"video/mp4", "video/webm", "video/ogg", "video/x-msvideo", "video/quicktime"}
     * )
     * @Vich\UploadableField(mapping="video", fileNameProperty="video", nullable=true)
     *
     * @var File $videofile
     */
    protected $videofile;

    /**
     * @var string $video
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     * @Expose
     */
    protected $video;

    /**
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={"application/pdf", "application/x-pdf"}
     * )
     * @Vich\UploadableField(mapping="pdfs", fileNameProperty="pdf", nullable=true)
     *
     * @var File $mipdf
     */
    private $mipdf;

    /**
     * @var string $pdf
     *
     * @ORM\Column(name="pdf", type="string", length=255, nullable=true)
     * @Expose
     */
    private $pdf;

    /**
     * @var string $youtube
     *
     * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
     * @Expose
     */
    private $youtube;

    /**
     * @ORM\ManyToMany(targetEntity="Material", inversedBy="instrucciones", cascade={"persist"})
     * @Expose
     */
    protected $materiales;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="Puesto", mappedBy="instruccion", cascade={"remove"})
     * @Expose
     */
    private $puestos;

    public function setMipdf($mipdf) {
        $this -> mipdf = $mipdf;
        if ($mipdf instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getMipdf() {
        return $this -> mipdf;
    }

    public function setImage($image) {
        $this -> image = $image;
        if ($image instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getImage() {
        return $this->image;
    }

    public function setVideofile($videofile) {
        $this -> videofile = $videofile;
        // if ($videofile instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        // }
    }

    public function getVideofile() {
        return $this-> videofile;
    }

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->getNombre();
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Instruccion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     * @return Instruccion
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Instruccion
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return Instruccion
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Instruccion
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     * @return Instruccion
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Instruccion
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Instruccion
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add materiales
     *
     * @param \Gitek\SuperlineaBundle\Entity\Material $materiales
     * @return Instruccion
     */
    public function addMateriale(\Gitek\SuperlineaBundle\Entity\Material $materiales)
    {
        $this->materiales[] = $materiales;

        return $this;
    }

    /**
     * Remove materiales
     *
     * @param \Gitek\SuperlineaBundle\Entity\Material $materiales
     */
    public function removeMateriale(\Gitek\SuperlineaBundle\Entity\Material $materiales)
    {
        $this->materiales->removeElement($materiales);
    }

    /**
     * Get materiales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMateriales()
    {
        return $this->materiales;
    }

    /**
     * Add puestos
     *
     * @param \Gitek\SuperlineaBundle\Entity\Puesto $puestos
     * @return Instruccion
     */
    public function addPuesto(\Gitek\SuperlineaBundle\Entity\Puesto $puestos)
    {
        $this->puestos[] = $puestos;

        return $this;
    }

    /**
     * Remove puestos
     *
     * @param \Gitek\SuperlineaBundle\Entity\Puesto $puestos
     */
    public function removePuesto(\Gitek\SuperlineaBundle\Entity\Puesto $puestos)
    {
        $this->puestos->removeElement($puestos);
    }

    /**
     * Get puestos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPuestos()
    {
        return $this->puestos;
    }

    /**
     * Set proceso
     *
     * @param string $proceso
     * @return Instruccion
     */
    public function setProceso($proceso)
    {
        $this->proceso = $proceso;
    
        return $this;
    }

    /**
     * Get proceso
     *
     * @return string 
     */
    public function getProceso()
    {
        return $this->proceso;
    }
}
