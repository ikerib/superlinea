<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\EntityRepository;

class InstruccionRepository extends EntityRepository
{
    public function instruccionessinasignar($productoid, $mytab) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT i FROM SuperlineaBundle:Instruccion i
                                    WHERE i.id NOT IN (
                                        SELECT ii FROM SuperlineaBundle:Instruccion ii
                                        JOIN ii.puestos p
                                        JOIN p.producto prd
                                        JOIN p.linea l
                                        WHERE prd.id = :productoid
                                        AND l.nombre = :mytab
                                        )
                                    ');

        $consulta->setParameter('productoid', $productoid);
        $consulta->setParameter('mytab', $mytab);

        return $consulta->getResult();

    }

    public function instruccionespatatablet($nombre, $fecha) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT i,p,prd,l,t,r FROM SuperlineaBundle:Instruccion i
                                        JOIN i.puestos p
                                        JOIN p.producto prd
                                        JOIN p.linea l
                                        JOIN p.tablet t
                                        JOIN prd.registros r
                                        WHERE r.fechafin > :fecha
                                        AND t.nombre = :nombre
                                    ');

//        $consulta = $em->createQuery('SELECT r, l , p, i, t FROM SuperlineaBundle:Registro r
//                                          JOIN r.lineas l
//                                          JOIN l.puestos p
//                                          JOIN p.instruccion i
//                                          JOIN p.tablet t
//                                          JOIN p.producto prd
//                                          WHERE r.fechafin > :fecha
//                                          AND t.nombre = :nombre
//        ');

        $consulta->setParameter('nombre', $nombre);
        $consulta->setParameter('fecha', $fecha);
        $consulta->setMaxResults(1);

//        print_r($nombre);
//        print_r("<br>");
//        print_r($fecha);
//        print_r("<br>");
//
//        print_r($consulta->getSql());
        return $consulta->getResult();

    }

    public function puesto($nombre, $fecha) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT p FROM SuperlineaBundle:Puesto p
                                    JOIN p.tablet t
                                    JOIN p.producto prd
                                    JOIN prd.registros r
                                    WHERE r.fecha = :fecha
                                    AND t.nombre = :nombre
                                    ORDER BY r.fecha DESC
                                    ');

        $consulta->setParameter('nombre', $nombre);
        $consulta->setParameter('fecha', $fecha);

        // $consulta->setMaxResults(1);

        // $resul = $consulta->getOneOrNullResult();
        $resul = $consulta->getResult();

        return $resul;
    }

    public function findReferencias() {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT DISTINCT i.referencia
                                    FROM SuperlineaBundle:Instruccion i
                                    ');

        $resul = $consulta->getResult();

        return $resul;
    }

    public function filtraReferencias($ref) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT i FROM SuperlineaBundle:Instruccion i
                                    WHERE i.referencia = :ref
                                    ');

        $consulta->setParameter('ref', $ref);

        $resul = $consulta->getResult();

        return $resul;
    }

}


