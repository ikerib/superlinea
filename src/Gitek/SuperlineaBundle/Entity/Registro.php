<?php

namespace Gitek\SuperlineaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\SuperlineaBundle\Entity\Registro
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\SuperlineaBundle\Entity\RegistroRepository")
 */
class Registro
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime $fecha
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime $fecha
     *
     * @ORM\Column(name="fechafin", type="datetime", nullable=true)
     */
    private $fechafin;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="registros")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToMany(targetEntity="Linea", inversedBy="registros", cascade={"persist"})
     */
    private $lineas;


    public function __construct()
    {
        // $this->fecha = new \DateTime();
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        // return $this->getProducto()->getNombre();
    }

    public function getCsrfIntention($intention)
    {
      return sha1(get_class($this).$intention.$this->id);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Registro
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Registro
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Registro
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set producto
     *
     * @param Gitek\SuperlineaBundle\Entity\Producto $producto
     * @return Registro
     */
    public function setProducto(\Gitek\SuperlineaBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return Gitek\SuperlineaBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Add lineas
     *
     * @param Gitek\SuperlineaBundle\Entity\Linea $lineas
     * @return Registro
     */
    public function addLinea(\Gitek\SuperlineaBundle\Entity\Linea $lineas)
    {
        $this->lineas[] = $lineas;

        return $this;
    }

    /**
     * Remove lineas
     *
     * @param Gitek\SuperlineaBundle\Entity\Linea $lineas
     */
    public function removeLinea(\Gitek\SuperlineaBundle\Entity\Linea $lineas)
    {
        $this->lineas->removeElement($lineas);
    }

    /**
     * Get lineas
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLineas()
    {
        return $this->lineas;
    }

    /**
     * Set fechafin
     *
     * @param \DateTime $fechafin
     * @return Registro
     */
    public function setFechafin($fechafin)
    {
        $this->fechafin = $fechafin;
    
        return $this;
    }

    /**
     * Get fechafin
     *
     * @return \DateTime 
     */
    public function getFechafin()
    {
        return $this->fechafin;
    }
}
