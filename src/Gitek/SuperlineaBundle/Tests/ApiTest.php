<?php

namespace Gitek\SuperlineaBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testGetInstrucciones()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/instrucciones/tablet2');

        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetPedidos()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/pedidos');

        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testPostPutPendientePedidos()
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/api/pedidos',
            array(
                'materialid' => '2',
                'puestoid' => '460',
                'productoid' => '1',
                'lineaid' => '1',
                'usuarioid' => '3'
            )
        );

        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertArrayHasKey('kolorea', $response);
        $this->assertArrayHasKey('pedidoid', $response);
        $this->assertArrayHasKey('materialfoto', $response);

        $pedidoid = $response['pedidoid'];

        $crawler = $client->request('GET', '/api/pedidopendiente/tablet2');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());


        $crawler = $client->request('PUT', '/api/pedidos/' . $pedidoid);
        $this->assertEquals(204, $client->getResponse()->getStatusCode(), "Hay que leer con el código de barras.");


        // Get de un pedido /pedidos/id
        $crawler = $client->request('GET', '/api/pedidos/' . $pedidoid);
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $pedido = $this->em
            ->getRepository('SuperlineaBundle:Pedido')
            ->find($pedidoid);

        $pedido->setLeido(1);
        $this->em->persist($pedido);
        $this->em->flush();

        $crawler = $client->request('PUT', '/api/pedidos/' . $pedidoid);
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Entregado.");


    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

}
