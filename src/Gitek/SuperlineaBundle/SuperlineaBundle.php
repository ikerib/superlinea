<?php

namespace Gitek\SuperlineaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SuperlineaBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
