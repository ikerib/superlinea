#SUPERLINEA


##Imagenes de materiales
----------------------------------
* Las imagenes de los materiales tienen que ser si o si archivos PNG:

## Configuración de las tablet
----------------------------------
* **URL:** http://DOMINIO/api/instrucciones/
* **Nombre dispositivo:** SuperlineatXXX
* **Dirección Video:** http://DOMINIO/video/
* **Dirección imagenes:** http://DOMINIO/images/uploads/
* **Carpeta Local Multimedia:** /SUPERLINEA/media/
* **URL Api Petición Material:** http://DOMINIO/api/pedidos
* **URL Api Check Pedido Pendiente:** http://DOMINIO/api/pedidopendiente/




###Otros
----------------------------------
#####Probando REST
  * curl -i -H "Accept: application/json" -X PUT -d "x=685,y=53,tabletid=8,instruccionid=9" http://superlinea.local/app_dev.php/api/puestos/444.json

  * Actualizar coordinadas PUT:
    * curl -i -H "Accept: application/json" -X PUT -d "instruccionid=&tabletid=3&usuarioid=&x=213&y=4" http://superlinea.dev/app_dev.php/api/puestos/459


#####Hacer que las imagenes del css funcionen en 'DEV'
  * RewriteRule ^app_dev\.php/(.*)/(.*)/img/(.*)$ /$1/$2/img/$3 [L]